import argparse
import random
import pyperclip
wordList = 'words.txt'


def get_random_line():
    words = open(wordList).read().splitlines()
    random_line = random.choice(words)
    return random_line


def generate_nickname(a, b, username=''):
    for x in range(a):
        tmp = get_random_line()
        username += tmp
    for x in range(b):
        tmp = str(random.randint(0, 9))
        username += tmp

    return username


parser = argparse.ArgumentParser(description='Generate usernames for throwaway/anonymous accounts')
parser.add_argument("-w", "--Words", required=False, type=int, default=2,
                    help="Number of words to generate for the username. (default: 1)")
parser.add_argument("-n", "--Numbers", required=False, type=int, default=2,
                    help="Number of integers added to the username (default: 2).")
parser.add_argument("-c", "--Copy", required=False, action='store_true',
                    help="Copy the generated username to clipboard..")
args = vars(parser.parse_args())
print(args)

if args['Copy']:
    generated = generate_nickname(args['Words'], args['Numbers'])
    print(generated)
    pyperclip.copy(generated)
    print('Copied to clipboard!')
else:
    generated = generate_nickname(args['Words'], args['Numbers'])
    print(generated)
